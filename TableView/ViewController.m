//
//  ViewController.m
//  TableView
//
//  Created by admin on 30/09/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "ViewController.h"
#import "TableViewController.h"

@interface ViewController ()

@end

@implementation ViewController{
    NSArray *table;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    table = [[NSArray alloc]initWithObjects:@"News",@"Movies",@"Sports",@"Restaurants",@"Malls", nil];
    
    UITableView *tableView = [[UITableView alloc]init];
    tableView.frame = CGRectMake(0, 30, 500, 500);
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [table count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *value = [table objectAtIndex:indexPath.row];

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:value];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:value];
    }
    
    cell.textLabel.text = [table objectAtIndex:indexPath.row];
    UILabel *label = [[UILabel alloc]init];
    label.text = [table objectAtIndex:indexPath.row];
    [cell addSubview:label];
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableViewController *tbc = [[TableViewController alloc]init];
    [self.navigationController pushViewController:tbc animated:YES];
}


    // Do any additional setup after loading the view, typically from a nib.


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
